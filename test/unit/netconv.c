#include <cldm/cldm.h>
#include <ulcer/ulcer.h>

#include <stdbool.h>
#include <stdint.h>

#include <arpa/inet.h>

TEST(ntohll) {
    uint64_t val = 0xdeadbeefabeull;
    if(ulcer_endianness() == ulcer_endian_big) {
        ASSERT_EQ(ulcer_ntohll(val), val);
    }
    else {
        ASSERT_EQ(ulcer_ntohll(val) >> 32u, ntohl(val & 0xffffffffu));
        ASSERT_EQ(ulcer_ntohll(val) & 0xffffffffu, ntohl(val >> 32u));
    }
}

TEST(htonll) {
    uint64_t val = 0xdeadbeefabeull;
    if(ulcer_endianness() == ulcer_endian_big) {
        ASSERT_EQ(ulcer_htonll(val), val);
    }
    else {
        ASSERT_EQ(ulcer_htonll(val) >> 32u, htonl(val & 0xffffffffu));
        ASSERT_EQ(ulcer_htonll(val) & 0xffffffffu, htonl(val >> 32u));
    }
}

TEST(ntoh) {
    uint8_t u8 = 0xdeu;
    uint16_t u16 = 0xdeadu;
    uint32_t u32 = 0xdeadbeefu;
    uint64_t u64 = 0xdeadbeefabeull;
    int8_t i8 = 0xd;
    int16_t i16 = 0xdea;
    int32_t i32 = 0xdeadbee;
    int64_t i64 = 0xdeadbeefab;
    ASSERT_EQ(ulcer_ntoh(u8), u8);
    ASSERT_EQ(ulcer_ntoh(u16), ntohs(u16));
    ASSERT_EQ(ulcer_ntoh(u32), ntohl(u32));
    ASSERT_EQ(ulcer_ntoh(u64), ulcer_ntohll(u64));
    ASSERT_EQ(ulcer_ntoh(i8), (uint8_t)i8);
    ASSERT_EQ(ulcer_ntoh(i16), ntohs((uint16_t)i16));
    ASSERT_EQ(ulcer_ntoh(i32), ntohl((uint32_t)i32));
    ASSERT_EQ(ulcer_ntoh(i64), ulcer_ntohll((uint64_t)i64));
}

TEST(hton) {
    uint8_t u8 = 0xdeu;
    uint16_t u16 = 0xdeadu;
    uint32_t u32 = 0xdeadbeefu;
    uint64_t u64 = 0xdeadbeefabeull;
    int8_t i8 = 0xd;
    int16_t i16 = 0xdea;
    int32_t i32 = 0xdeadbee;
    int64_t i64 = 0xdeadbeefab;
    ASSERT_EQ(ulcer_hton(u8), u8);
    ASSERT_EQ(ulcer_hton(u16), htons(u16));
    ASSERT_EQ(ulcer_hton(u32), htonl(u32));
    ASSERT_EQ(ulcer_hton(u64), ulcer_htonll(u64));
    ASSERT_EQ(ulcer_hton(i8), (uint8_t)i8);
    ASSERT_EQ(ulcer_hton(i16), htons((uint16_t)i16));
    ASSERT_EQ(ulcer_hton(i32), htonl((uint32_t)i32));
    ASSERT_EQ(ulcer_hton(i64), ulcer_htonll((uint64_t)i64));
}
