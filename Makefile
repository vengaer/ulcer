archive           := libulcer.a
solib             := libulcer.so
sover             := 0
socompat          := 0

test              := $(patsubst lib%.so,%,$(solib))test
fuzz              := $(patsubst lib%.so,%,$(solib))fuzz

root              := $(abspath $(CURDIR))
srcdir            := $(root)/src
testdir           := $(root)/test/unit
fuzzdir           := $(root)/test/fuzz

builddir          := build
fuzzbuilddir      := $(builddir)/fuzz

cldm              := cldm/libcldm.so
cldmdir           := $(dir $(cldm))

CLDMFLAGS         := -vj$(shell nproc)

CC                ?= gcc
AR                ?= ar
FUZZCC            := clang
LN                ?= ln
MKDIR             ?= mkdir
RM                ?= rm
TOUCH             ?= touch

CFLAGS            := -Wall -Wextra -Wpedantic -std=c11 -g -fPIC -MD -MP -c
CPPFLAGS          := -I$(root)
LDFLAGS           := -shared -Wl,-soname,$(solib).$(socompat)
LDLIBS            :=
ARFLAGS           := -rc

TESTCFLAGS        := $(CFLAGS)
TESTCPPFLAGS      := -I$(cldmdir) $(CPPFLAGS)
TESTLDFLAGS       := -L. -L$(cldmdir)
TESTLDLIBS        := -lcldm -lcldm_main -lulcer

FUZZCFLAGS        := $(CFLAGS) -fsanitize=fuzzer,address,undefined -fprofile-instr-generate -fcoverage-mapping
FUZZCPPFLAGS      := $(CPPFLAGS)
FUZZLDFLAGS       := -L. $(filter-out $(CFLAGS),$(FUZZCFLAGS))
FUZZLDLIBS        :=

FUZZTIME          ?= 240
FUZZLEN           ?= 4096
FUZZVALPROF       ?= 1
FUZZTIMEO         ?= 10
FUZZCORPORA       := $(fuzzdir)/corpora
FUZZFLAGS         := -max_len=$(FUZZLEN) -max_total_time=$(FUZZTIME) -use_value_profile=$(FUZZVALPROF) \
                     -timeout=$(FUZZTIMEO) $(FUZZCORPORA)
FUZZMERGEFLAGS    := -merge=1 $(FUZZCORPORA) $(CORPORA)

LLVM_PROFILE_FILE := $(builddir)/.fuzz.profraw
PROFDATA          := $(builddir)/.fuzz.profdata
PROFFLAGS         := merge -sparse $(LLVM_PROFILE_FILE) -o $(PROFDATA)

COVFLAGS          := show $(fuzz) -instr-profile=$(PROFDATA)
COVREPFLAGS       := report $(fuzz) -instr-profile=$(PROFDATA)

LNFLAGS           := -sf
MKDIRFLAGS        := -p
RMFLAGS           := -rf

QUIET             := @

cext              := c
oext              := o

obj               := $(patsubst $(srcdir)/%.$(cext),$(builddir)/%.$(oext),$(wildcard $(srcdir)/*.$(cext)))
testobj           := $(patsubst $(testdir)/%.$(cext),$(builddir)/%.$(oext),$(wildcard $(testdir)/*.$(cext)))
fuzzobj           := $(patsubst $(srcdir)/%.$(cext),$(builddir)/fuzz/%.$(oext),$(wildcard $(srcdir)/*.$(cext))) \
                     $(patsubst $(fuzzdir)/%.$(cext),$(builddir)/fuzz/%.$(oext),$(wildcard $(fuzzdir)/*.$(cext)))

cldm_init         := $(builddir)/.cldm.init.stamp

export LLVM_PROFILE_FILE

.PHONY: all
all: $(archive) $(solib)

$(archive): $(obj)
	$(info [AR] $@)
	$(QUIET)$(AR) $(ARFLAGS) -o $@ $^

$(solib): $(solib).$(sover)
	$(info [LN] $@)
	$(QUIET)$(LN) $(LNFLAGS) $(abspath $(CURDIR))/$^ $@

$(solib).$(sover): $(obj)
	$(info [LD] $@)
	$(QUIET)$(CC) -o $@ $^ $(LDFLAGS) $(LDLIBS)

$(builddir)/%.$(oext): $(srcdir)/%.$(cext) | $(builddir)
	$(info [CC] $(notdir $@))
	$(QUIET)$(CC) -o $@ $< $(CFLAGS) $(CPPFLAGS)

$(builddir)/%.$(oext): $(testdir)/%.$(cext) $(cldm) | $(builddir)
	$(info [CC] $(notdir $@))
	$(QUIET)$(CC) -o $@ $< $(TESTCFLAGS) $(TESTCPPFLAGS)

$(builddir)/fuzz/%.$(oext): $(fuzzdir)/%.$(cext) | $(fuzzbuilddir)
	$(info [CC] $(notdir $@))
	$(QUIET)$(FUZZCC) -o $@ $< $(FUZZCFLAGS) $(FUZZCPPFLAGS)

$(builddir)/fuzz/%.$(oext): $(srcdir)/%.$(cext) | $(fuzzbuilddir)
	$(info [CC] $(notdir $@))
	$(QUIET)$(FUZZCC) -o $@ $< $(FUZZCFLAGS) $(FUZZCPPFLAGS)

$(cldm_init): | $(builddir)
	$(QUIET)git submodule update --init
	$(QUIET)sed -i 's/std=c99/std=c11/' $(cldmdir)/Makefile
	$(QUIET)$(TOUCH) $@

$(cldm): $(cldm_init)
	$(QUIET)$(MAKE) -j$(nproc) -C $(cldmdir)

$(test): $(testobj) $(cldm) $(solib)
	$(info [LD] $@)
	$(QUIET)$(CC) -o $@ $(testobj) $(TESTLDFLAGS) $(TESTLDLIBS)

$(fuzz): $(fuzzobj)
	$(info [LD] $@)
	$(QUIET)$(FUZZCC) -o $@ $^ $(FUZZLDFLAGS) $(FUZZLDLIBS)

.PHONY: test
test: $(test)

.PHONY: fuzz
fuzz: $(fuzz)

.PHONY: check
check: $(test)
	$(QUIET)LD_LIBRARY_PATH=.:$(dir $(cldm)) valgrind ./$^ $(CLDMFLAGS)

.PHONY: fuzzrun
fuzzrun: $(fuzz)
	$(QUIET)./$^ $(FUZZFLAGS)
	$(QUIET)llvm-profdata $(PROFFLAGS)
	$(QUIET)llvm-cov $(COVFLAGS)
	$(QUIET)llvm-cov $(COVREPFLAGS)

define require-corpora
$(if $(findstring -_-$(MAKECMDGOALS)-_-,-_-$(1)-_-),$(if $(CORPORA),,$(error CORPORA is empty)))
endef

.PHONY: fuzzmerge
fuzzmerge: $(call require-corpora,fuzzmerge)
fuzzmerge: $(fuzz)
	$(QUIET)./$^ $(FUZZMERGEFLAGS)

$(builddir):
	$(QUIET)$(MKDIR) $(MKDIRFLAGS) $@

$(fuzzbuilddir):
	$(QUIET)$(MKDIR) $(MKDIRFLAGS) $@

.PHONY: clean
clean:
	$(QUIET)$(RM) $(RMFLAGS) $(builddir) $(solib) $(solib).$(sover) $(test) $(fuzz) $(archive)

.PHONY: distclean
distclean: clean
	$(QUIET)$(MAKE) -C $(dir $(cldm)) clean
