#include <ulcer/ulcer.h>

#include <string.h>

uint8_t ulcer_identity8(uint8_t val);
ulcer_buftype *ulcer_init(struct ulcer_buflayout *layout, size_t cap);
ulcer_buftype *ulcer_init_raw(unsigned char *raw, size_t cap);

bool ulcer_nwpack8(ulcer_buftype *restrict buf, uint8_t v);
bool ulcer_nwpack16(ulcer_buftype *restrict buf, uint16_t v);
bool ulcer_nwpack32(ulcer_buftype *restrict buf, uint32_t v);
bool ulcer_nwpack64(ulcer_buftype *restrict buf, uint64_t v);

bool ulcer_nwunpack8(ulcer_buftype *restrict buf, uint8_t *restrict v);
bool ulcer_nwunpack16(ulcer_buftype *restrict buf, uint16_t *restrict v);
bool ulcer_nwunpack32(ulcer_buftype *restrict buf, uint32_t *restrict v);
bool ulcer_nwunpack64(ulcer_buftype *restrict buf, uint64_t *restrict v);

enum ulcer_endian ulcer_endianness(void);

#define pack(buf, v)                                                \
    ulcer_size(buf) + sizeof(v) <= (size_t)ulcer_bufcap(buf) &&     \
    (memcpy(buf + ulcer_size(buf), &(v), sizeof(v)),                \
    ulcer_size(buf) += sizeof(v), true)

#define unpack(buf, v)                                              \
    ulcer_rdpos(buf) + sizeof(*(v)) <= (size_t)ulcer_size(buf) &&   \
    (memcpy(v, buf + ulcer_rdpos(buf), sizeof(*(v))),               \
    ulcer_rdpos(buf) += sizeof(*(v)), true)

static inline bool ulcer_packarr8(ulcer_buftype *restrict buf, struct ulcer_arraytype const *restrict arr) {
    bool success = true;
    for(size_t i = 0; i < arr->size && success; i++) {
        success &= ulcer_pack8(buf, ((uint8_t *)arr->addr)[i]);
    }
    return success;
}

static inline bool ulcer_packarr16(ulcer_buftype *restrict buf, struct ulcer_arraytype const *restrict arr) {
    bool success = true;
    for(size_t i = 0; i < arr->size && success; i++) {
        success &= ulcer_pack16(buf, ((uint16_t *)arr->addr)[i]);
    }
    return success;
}

static inline bool ulcer_packarr32(ulcer_buftype *restrict buf, struct ulcer_arraytype const *restrict arr) {
    bool success = true;
    for(size_t i = 0; i < arr->size && success; i++) {
        success &= ulcer_pack32(buf, ((uint32_t *)arr->addr)[i]);
    }
    return success;
}

static inline bool ulcer_packarr64(ulcer_buftype *restrict buf, struct ulcer_arraytype const *restrict arr) {
    bool success = true;
    for(size_t i = 0; i < arr->size && success; i++) {
        success &= ulcer_pack64(buf, ((uint64_t *)arr->addr)[i]);
    }
    return success;
}

static inline bool ulcer_nwpackarr8(ulcer_buftype *restrict buf, struct ulcer_arraytype const *restrict arr) {
    bool success = true;
    for(size_t i = 0; i < arr->size && success; i++) {
        success &= ulcer_nwpack8(buf, ((uint8_t *)arr->addr)[i]);
    }
    return success;
}

static inline bool ulcer_nwpackarr16(ulcer_buftype *restrict buf, struct ulcer_arraytype const *restrict arr) {
    bool success = true;
    for(size_t i = 0; i < arr->size && success; i++) {
        success &= ulcer_nwpack16(buf, ((uint16_t *)arr->addr)[i]);
    }
    return success;
}

static inline bool ulcer_nwpackarr32(ulcer_buftype *restrict buf, struct ulcer_arraytype const *restrict arr) {
    bool success = true;
    for(size_t i = 0; i < arr->size && success; i++) {
        success &= ulcer_nwpack32(buf, ((uint32_t *)arr->addr)[i]);
    }
    return success;
}

static inline bool ulcer_nwpackarr64(ulcer_buftype *restrict buf, struct ulcer_arraytype const *restrict arr) {
    bool success = true;
    for(size_t i = 0; i < arr->size && success; i++) {
        success &= ulcer_nwpack64(buf, ((uint64_t *)arr->addr)[i]);
    }
    return success;
}

static inline bool ulcer_unpackarr8(ulcer_buftype *restrict buf, struct ulcer_arraytype *restrict arr) {
    bool success = true;
    for(size_t i = 0; i < arr->size && success; i++) {
        success &= ulcer_unpack8(buf, &((uint8_t *)arr->addr)[i]);
    }
    return success;
}

static inline bool ulcer_unpackarr16(ulcer_buftype *restrict buf, struct ulcer_arraytype *restrict arr) {
    bool success = true;
    for(size_t i = 0; i < arr->size && success; i++) {
        success &= ulcer_unpack16(buf, &((uint16_t *)arr->addr)[i]);
    }
    return success;
}

static inline bool ulcer_unpackarr32(ulcer_buftype *restrict buf, struct ulcer_arraytype *restrict arr) {
    bool success = true;
    for(size_t i = 0; i < arr->size && success; i++) {
        success &= ulcer_unpack32(buf, &((uint32_t *)arr->addr)[i]);
    }
    return success;
}

static inline bool ulcer_unpackarr64(ulcer_buftype *restrict buf, struct ulcer_arraytype *restrict arr) {
    bool success = true;
    for(size_t i = 0; i < arr->size && success; i++) {
        success &= ulcer_unpack64(buf, &((uint64_t *)arr->addr)[i]);
    }
    return success;
}

static inline bool ulcer_nwunpackarr8(ulcer_buftype *restrict buf, struct ulcer_arraytype *restrict arr) {
    bool success = true;
    for(size_t i = 0; i < arr->size && success; i++) {
        success &= ulcer_nwunpack8(buf, &((uint8_t *)arr->addr)[i]);
    }
    return success;
}

static inline bool ulcer_nwunpackarr16(ulcer_buftype *restrict buf, struct ulcer_arraytype *restrict arr) {
    bool success = true;
    for(size_t i = 0; i < arr->size && success; i++) {
        success &= ulcer_nwunpack16(buf, &((uint16_t *)arr->addr)[i]);
    }
    return success;
}

static inline bool ulcer_nwunpackarr32(ulcer_buftype *restrict buf, struct ulcer_arraytype *restrict arr) {
    bool success = true;
    for(size_t i = 0; i < arr->size && success; i++) {
        success &= ulcer_nwunpack32(buf, &((uint32_t *)arr->addr)[i]);
    }
    return success;
}

static inline bool ulcer_nwunpackarr64(ulcer_buftype *restrict buf, struct ulcer_arraytype *restrict arr) {
    bool success = true;
    for(size_t i = 0; i < arr->size && success; i++) {
        success &= ulcer_nwunpack64(buf, &((uint64_t *)arr->addr)[i]);
    }
    return success;
}

uint64_t ulcer_ntohll(uint64_t hostll) {
    bool sf = !ulcer_endianness();
    return ((uint64_t)ntohl(hostll & 0xffffffffu) << (32u * sf)) | ntohl(hostll >> (32u * sf));
}

uint64_t ulcer_htonll(uint64_t netll) {
    bool sf = !ulcer_endianness();
    return ((uint64_t)htonl(netll & 0xffffffffu) << (32u * sf)) | htonl(netll >> (32u * sf));
}

bool ulcer_pack8(ulcer_buftype *restrict buf, uint8_t v) {
    return pack(buf, v);
}

bool ulcer_pack16(ulcer_buftype *restrict buf, uint16_t v) {
    return pack(buf, v);
}

bool ulcer_pack32(ulcer_buftype *restrict buf, uint32_t v) {
    return pack(buf, v);
}

bool ulcer_pack64(ulcer_buftype *restrict buf, uint64_t v) {
    return pack(buf, v);
}

bool ulcer_packarr(ulcer_buftype *restrict buf, struct ulcer_arraytype const *restrict arr) {
    switch(arr->elemsize) {
        case sizeof(uint8_t):
            return ulcer_packarr8(buf, arr);
        case sizeof(uint16_t):
            return ulcer_packarr16(buf, arr);
        case sizeof(uint32_t):
            return ulcer_packarr32(buf, arr);
        case sizeof(uint64_t):
            return ulcer_packarr64(buf, arr);
        default:
            break;
    }
    ulcer_panic("Invalid element size: array packing supported only for integral types");
}

bool ulcer_nwpackarr(ulcer_buftype *restrict buf, struct ulcer_arraytype const *restrict arr) {
    switch(arr->elemsize) {
        case sizeof(uint8_t):
            return ulcer_nwpackarr8(buf, arr);
        case sizeof(uint16_t):
            return ulcer_nwpackarr16(buf, arr);
        case sizeof(uint32_t):
            return ulcer_nwpackarr32(buf, arr);
        case sizeof(uint64_t):
            return ulcer_nwpackarr64(buf, arr);
        default:
            break;
    }
    ulcer_panic("Invalid element size: array packing supported only for integral types");
}

bool ulcer_unpack8(ulcer_buftype *restrict buf, uint8_t *restrict v) {
    return unpack(buf, v);
}

bool ulcer_unpack16(ulcer_buftype *restrict buf, uint16_t *restrict v) {
    return unpack(buf, v);
}

bool ulcer_unpack32(ulcer_buftype *restrict buf, uint32_t *restrict v) {
    return unpack(buf, v);
}

bool ulcer_unpack64(ulcer_buftype *restrict buf, uint64_t *restrict v) {
    return unpack(buf, v);
}

bool ulcer_unpackarr(ulcer_buftype *restrict buf, struct ulcer_arraytype *restrict arr) {
    switch(arr->elemsize) {
        case sizeof(uint8_t):
            return ulcer_unpackarr8(buf, arr);
        case sizeof(uint16_t):
            return ulcer_unpackarr16(buf, arr);
        case sizeof(uint32_t):
            return ulcer_unpackarr32(buf, arr);
        case sizeof(uint64_t):
            return ulcer_unpackarr64(buf, arr);
        default:
            break;
    }
    ulcer_panic("Invalid element size: array packing supported only for integral types");
}

bool ulcer_nwunpackarr(ulcer_buftype *restrict buf, struct ulcer_arraytype *restrict arr) {
    switch(arr->elemsize) {
        case sizeof(uint8_t):
            return ulcer_nwunpackarr8(buf, arr);
        case sizeof(uint16_t):
            return ulcer_nwunpackarr16(buf, arr);
        case sizeof(uint32_t):
            return ulcer_nwunpackarr32(buf, arr);
        case sizeof(uint64_t):
            return ulcer_nwunpackarr64(buf, arr);
        default:
            break;
    }
    ulcer_panic("Invalid element size: array packing supported only for integral types");
}
