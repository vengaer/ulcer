FROM archlinux:latest
LABEL maintainer="vilhelm.engstrom@tuta.io"

RUN pacman -Syu --needed --noconfirm make clang gcc git llvm valgrind

ENV CC=gcc

COPY . /ulcer
WORKDIR /ulcer
